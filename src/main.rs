use bevy::{log::LogPlugin, prelude::*};

fn main() {
    App::new()
        .add_plugin(DebugPlugin)
        .add_startup_system(hello_world_system)
        .add_startup_system(test_log_system)
        .run();
}

/// A simple sustem that prints "Hello World!"
fn hello_world_system() {
    println!("Hello World!")
}

fn test_log_system() {
    error!("Error!");
    warn!("Warning!");
    info!("Info!");
    debug!("Debug!");
    trace!("Trace!");
}

/// Plugin to manage what gets loaded during debug and release.
struct DebugPlugin;

impl Plugin for DebugPlugin {
    fn build(&self, app: &mut App) {
        #[cfg(debug_assertions)] // This block is not compiled when not debugging.
        {
            println!("Debug on!");
            app.add_plugins(DefaultPlugins.set(LogPlugin {
                level: bevy::log::Level::DEBUG,
                filter: "info,wgpu_core=warn,wgpu_hal=warn,simple_2d_game=debug".into(),
            }));
        }

        #[cfg(not(debug_assertions))] // This block is not compiled during debugging.
        {
            app.add_plugins(DefaultPlugins);
        }
    }
}
